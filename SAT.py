# dimacs.py - Provides implementations of algorithms 
#             walkSAT, GSAT and DPLL
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC
#

import random

##### walkSAT #####
# [purpose]: Implementation of the walkSAT algorithm.
#
# [input]:
#
#   sentence - tuple with the format (clauses, N, C),
#   where clauses is a list of lists of literals
#   [[l0, l1, l2], ... , [l0, l1, l2]]
#
#   p - probability of choosing flipRandomSymbol over
#   flipBestSymbol
#
#   max_flips - maximum number of symbol flips the
#   the algorithm will do before forfeiting
#
# [output]: Tuple (isSat, sol, seed, flips) where:
#
#   isSat - True if solution was found, False otherwise
#   sol - model of the solution
#   seed - seed used in the random run
#   flips - number of flips until solution was found
#   or number of max_flips reached
#
def walkSAT(sentence, p, max_flips):

    # Randomize a random seed and store it
    # to enable algorithm reprodution
    seed = int(random.random()*100)
    random.seed(seed)
    model = randomModel(sentence)
    
    for i in range(max_flips):

        # Check if the sentence is satisfied (sentence[0]=clauses)
        if isSatisfied(sentence[0], model):
            return (True,model,seed,i)

        # Get Randomly selected Clause which is false in model
        clause = randomFalseClause(sentence, model)
        
        ps = random.random()
        if ps <= p:
            # Flip Random Symbol from clause
            model = flipRandomSymbol(clause, model)
        else:
            # Flip Symbol from clause that maximizes number of satisfied clauses in sentence
            model = flipBestSymbol(sentence, clause, model)
            
    return False,None,seed,max_flips

##### GSAT #####
# [purpose]: Implementation of the GSAT algorithm.
#
# [input]:
#
#   sentence - tuple with the format (clauses, N, C),
#   where clauses is a list of lists of literals
#   [[l0, l1, l2], ... , [l0, l1, l2]]
#
#   max_restarts - number of maximum restarts the
#   the algorithm will do.
#
#   max_climbs - maximum number of climbs the algorithm
#   will do per restart.
#
# [output]: Tuple (isSat, sol, seed, restarts, climbs) where:
#
#   isSat - True if solution was found, False otherwise
#   sol - model of the solution
#   seed - seed used in the random run
#   restarts - number of restarts until solution was found
#       or number of max_restarts reached (algorithm forfeited)
#   climbs - number of climbs until solution was found
#       or number of max_climbs if max_restarts is reached (algorithm forfeited)
def GSAT(sentence, max_restarts, max_climbs):

    seed = int(random.random()*100)
    random.seed(seed)
            
    for i in range(max_restarts):
                
        model = randomModel(sentence)

        for j in range(max_climbs):
            if isSatisfied(sentence[0], model):
                return (True,model,seed,i,j)
            model = getBestSuccessor(sentence, model)

        if isSatisfied(sentence[0], model):
            return (True,model,seed,i,j)
            
    return False,None,seed,max_restarts,max_climbs

##### DPLL_SAT #####
# [purpose]:
#
#   This function implements the DPLL algorithm.
#   The symbols list is built here, as well as unpacking of the
#   sentence Tuple.From this function, the first recursive
#   DPLL call is made.
#
# [input]:
#
#   sentence - tuple with the format (clauses, N, C),
#   where clauses is a list of lists of literals
#   [[l0, l1, l2], ... , [l0, l1, l2]]
#
# [output]: model if the solution was found, None otherwise.
#   
def DPLL_SAT(sentence):

    clauses = sentence[0]
    nVar = sentence[1]
    
    symbols = list (range(1, nVar+1))

    model = {}
    
    return DPLL(clauses, symbols, model)

##### DPLL #####
# [purpose]: This function is a recursive implementation of DPLL.
#
# [input]:
#
#   sentence - tuple with the format (clauses, N, C),
#   where clauses is a list of lists of literals
#   [[l0, l1, l2], ... , [l0, l1, l2]]
#
# [output]: model if the solution was found, None otherwise.
#   
def DPLL(clauses, symbols, model):
    
    # Check For satisfiability
    isSat = isSatisfied(clauses, model)

    # Check if conclusions can be drawn
    if isSat is not None:
        if isSat == True:
            return model
        elif isSat == False:
            return False
    
    # Pure Symbols
    P, value = FindPureSymbol(symbols, clauses, model)
    
    if P is not None:
        
        # Add P=value to model
        model[P] = value
        
        # Remove P from symbols
        symbols.remove(P)

        return DPLL(clauses, symbols, model)

    # Unit Clauses
    P, value = FindUnitClause(clauses, model)

    if P is not None:
        
        # Add P=value to model
        model[P] = value

        # Remove P from symbols
        symbols.remove(P)

        return DPLL(clauses, symbols, model)

    # Branch left->P=false , right->P=true

    # Copy model for both branches
    model_left = dict(model)
    model_right = dict(model)

    # Copy symbols for both branches
    symbols_left = list(symbols)
    symbols_right = list(symbols)

    # Remove symbol P from both symbols lists
    P = symbols_left.pop()
    symbols_right.remove(P)

    # left->P=false , right->P=true
    model_left[P] = True
    model_right[P] = False

    # The second call is only executed if the first one fails
    return DPLL(clauses, symbols_left, model_left) \
           or DPLL(clauses, symbols_right, model_right)

##### FindUnitClause #####
# [purpose]: This function finds a unit clause in a list
#   of clauses, if there is one.
#
#   The function sweeps each clause to find out if it is
#   a unit clause. For each one, count the number of assignments
#   that evaluate to False and the number of None assignments
#   (not yet assigned). The clause is a unit clause if there are exactly
#   len(clause)-1 (2 for 3-SAT) False symbols and one
#   Unassigned symbol.
#
# [input]:
#
#   clauses - a list of lists of literals
#   [[l0, l1, l2], ... , [l0, l1, l2]]
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: Tuple (unitSymbol, unitSymbolValue), where:
#
#   unitSymbol - symbol corresponding to the unit clause
#   unitSymbolValue - value that should be assigned to symbol
#   for the clause to evaluate True.
#   If no unit clause is found, the function returns (None,None)
#  
def FindUnitClause(clauses, model):

    for clause in clauses:

        FalseCount = 0
        NoneCount = 0
        isSat = False

        # Sweep each clause to find out if it is a unit clause.
        for literal in clause: 
            symbol = abs(literal)
            assignment = model.get(symbol)

            # If the symbol already as an assignement check
            # if it evaluates To true or False. It it is
            # True, clause is satisfied and may be ignored,
            # otherwise, count the number of False occurrences.
            if assignment is not None:

                if literal < 0: # Negation of literal

                    if assignment == False: # Clause is already satisfied. 
                        isSat = True
                        break # Ignore clause
                    else:
                        FalseCount+=1 # Count False
                        
                else: # Positive Literal
                    
                    if assignment == True: # Clause is already satisfied   
                        isSat = True
                        break # Ignore clause
                    else:
                        FalseCount+=1 # Count False
                    
            else: # Unasigned symbol was found. Count it.
                NoneCount+=1
                unitSymbol = symbol

                # Choose the assignment this symbol should get 
                if literal < 0:
                    unitSymbolValue = False
                else:
                    unitSymbolValue = True
                
        # Check if this is a unit clause. This is true if:
        # There are exactly len(clause)-1 (2 for 3-SAT) False symbols and one Unassigned symbol.
        if not isSat:
            if FalseCount == (len(clause)-1) and NoneCount==1: # Unit and unassigned symbol
                return unitSymbol, unitSymbolValue

    # No unit clause was found       
    return None, None

##### FindPureSymbol #####
# [purpose]: This function finds a pure symbol in a list
#   of clauses, if there is one. Satisfied clauses are ignored
#   in deciding if a symbol is pure or not.
#
# [input]:
#
#   symbols - list of unassigned symbols [si, ... , sj]
#
#   clauses - a list of lists of literals
#   [[l0, l1, l2], ... , [l0, l1, l2]]
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: Tuple (pureSymbol, pureSymbolValue), where:
#
#   pureSymbol - pure symbol found
#   pureSymbolValue - value that should be assigned to symbol
#   for the clause to evaluate True.
#   If no symbol is found, the function returns (None,None)
#  
def FindPureSymbol(symbols, clauses, model):

    # By default all symbols are pure, without any signal assignment
    # This list is updated during the execution of the function
    pureSymbols = list(symbols)
    pureSignals = [0]*max(symbols)
    
    # Sweep clauses for pure symbols
    for clause in clauses:

        # Ignore True clauses 
        if isTrueClause(clause, model):
            continue

        # Check if something can be concluded about the unpurity of each symbol
        for literal in clause:
            
            symbol = abs(literal)
            
            # If symbol is known to be unpure or was not in the given symbols list, ignore it.
            if not pureSymbols.__contains__(symbol):
                continue

            # Collect the index of the symbol
            pureIndex = symbol-1
            pureSignal = pureSignals[pureIndex]

            # Depending on the signal of the literal, check if the symbol is unpure
            # (according to previous signals stored in the list pureSignals) or if
            # it remains pure.
            if literal < 0: # Negation of literal
                
                if(pureSignal==0): # If no signal yet, assign '<0'
                    pureSignals[pureIndex]=-1
           
                elif(pureSignal==1): # If it was assigned '>0' before, it's not pure.
                    pureSymbols.remove(symbol)
                    
            else:           # Positive of literal

                if(pureSignal==0): # If no signal yet, assign '>0'
                    pureSignals[pureIndex]=1
                    
                elif(pureSignal==-1): # If it was assigned '<0' before, it's not pure.
                    pureSymbols.remove(symbol)

    if pureSymbols: # If the list is not empty

        # Sweep for Pure symbols which were in the sweeped clauses (pureSignal != 0)
        # The symbols that were not in the sweeped clauses are only in satisfied
        # clauses, and so are of no interest.
        assignedPureSymbol = False        
        for candidateSymbol in pureSymbols:
            pureIndex = candidateSymbol-1
            pureSignal =  pureSignals[pureIndex]
            
            if pureSignal != 0: # Found pure symbol 
                pureSymbol = candidateSymbol
                assignedPureSymbol = True
                break
        
        # Choose symbol assignment according to signal
        if assignedPureSymbol:
            if pureSignal > 0:
                return pureSymbol, True
            else:
                return pureSymbol, False
            
        else:# There are no pure symbols left in the given symbols list, which are in unsat clauses
            return None, None
      
    else: # There are no pure symbols left in the given symbols list
        return None, None

##### FindPureSymbol #####
# [purpose]: Find out if the sentence composed by clauses is satisfied
#   by the current model. This function calls isTrueClause for each clause in clauses.
#
# [input]:
#
#   clauses - a list of lists of literals
#   [[l0, l1, l2], ... , [l0, l1, l2]]
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: True if the sentence is satisfied, False otherwise.
#  
def isSatisfied(clauses, model):
    
    for clause in clauses:
        isSat = isTrueClause(clause, model)
        if isSat is None:
            return None
        elif not isSat:
            return False
        
    return True

##### isTrueClause #####
# [purpose]: Find out if a each clause is satisfied by the current model.
#
# [input]:
#
#   clause - list of literals [l0, l1, l2]
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: True if the clause is satisfied, False otherwise.
#  
def isTrueClause(clause, model):
	
    clauseLogic = False
    NoneCount = 0
    
    for literal in clause:

        symbol = abs(literal)
        assignment = model.get(symbol)

        # If there is an assignment for the symbol in the current model:
        #   >If it evaluates True, clause is True and cycle may be stoped.
        #   >If it evaluates False, no conclusions can be drawn yet.
        # If there is no assignment for the symbol, count the number of unknown assignments.
        if assignment is not None:
           
            if literal < 0: # Negation of literal
                if assignment == False:
                    clauseLogic = True
                    break                    
            else:           # Positive literal
                if assignment == True:
                    clauseLogic = True
                    break
                
        else: # Nothing is known about the symbol
            NoneCount += 1

    # Check if the drawn conclusions are valid:
    #   >If All symbols evaluate False, the clause is False.
    #   >If some symbol evaluates True, the clause is True.
    if NoneCount == 0 and clauseLogic == False:
        return False
    elif clauseLogic == True:
        return True
    else:
        return None

##### randomModel #####
# [purpose]: Generate a random model for a given sentence.
#
# [input]:
#
#   sentence - Tuple (clauses, N, C)
#
#       clauses - a list of lists of literals
#       [[l0, l1, l2], ... , [l0, l1, l2]]
#
#       N - number of variables
#
#       C - number of clauses
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: variableAssignments - the random model.
#  
def randomModel(sentence):

    clauses = sentence[0]
    nVar = sentence[1]
    nClause = sentence[2]

    variableAssignments = {}
    
    for i in range(1, nVar+1):
        
        choice = False
        
        p = random.random();

        if p > 0.5:
            choice = True

        variableAssignments[i] = choice

    return variableAssignments

##### getBestSuccessor #####
# [purpose]: Randomly choose a model amoung the best successors
#   of the provided model. The successors are obtained by fliping
#   the assignment of variable in the provided model and classified
#   according to the number of satisfied sentences.
#
# [input]:
#
#   sentence - Tuple (clauses, N, C)
#
#       clauses - a list of lists of literals
#       [[l0, l1, l2], ... , [l0, l1, l2]]
#
#       N - number of variables
#
#       C - number of clauses
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: Chosen successor model.
#
def getBestSuccessor(sentence, model):

    clauses = sentence[0]
    nVar = sentence[1]
    nClause = sentence[2]

    bestSuccessors = []
    maxSatClauses = 0

    for var in range (1,nVar+1): # For all successors

        # Generate a successor with a fliped symbol
        successorModel = dict(model)
        successorModel[var] = not model[var]

        # Check how many clauses the successor satisfies
        satClauses = 0
        for clause in clauses:
            if isTrueClause(clause, successorModel):
                satClauses+=1

        # If solution is found, return imediately
        if(satClauses == nClause):
            return successorModel
            
        # Check if successor may join the current list of best successors
        # Or if it is good enough to kick the others from the list

        if(satClauses > maxSatClauses):
            bestSuccessors.clear()
            bestSuccessors.append(successorModel)
            maxSatClauses = satClauses
        elif (satClauses == maxSatClauses):
            bestSuccessors.append(successorModel)

    return random.choice(bestSuccessors)
        

def randomFalseClause(sentence, model):
    
    clauses = sentence[0]

    # List false clauses in model
    falseClauses = []
    for clause in clauses:
        if not isTrueClause(clause, model):
            falseClauses.append(clause)

    # Choose and return a false clause
    return random.choice(falseClauses)
    

##### flipBestSymbol #####
# [purpose]: Flip Symbol from clause that maximizes
#   number of satisfied clauses in sentence
#
# [input]:
#
#   sentence - Tuple (clauses, N, C)
#
#       clauses - a list of lists of literals
#       [[l0, l1, l2], ... , [l0, l1, l2]]
#
#       N - number of variables
#
#       C - number of clauses
#
#   selectedClause - clause from which the symbol
#   will be selected
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: Chosen successor model.
#
def flipBestSymbol(sentence, selectedClause, model):

    clauses = sentence[0]
    nClause = sentence[2]

    bestSuccessors = []
    maxSatClauses = 0
    
    # For every literal in the selected clause
    for literal in selectedClause:

        # Evaluate the symbolIndex
        symbolIndex = abs(literal)

        # Flip symbol in successorModel
        successorModel = dict(model)
        successorModel[symbolIndex] = not successorModel[symbolIndex]

        # Check how many clauses the successor satisfies
        satClauses = 0
        for clause in clauses:
            if isTrueClause(clause, successorModel):
                satClauses+=1

        # If solution is found, return imediately
        if(satClauses == nClause):
            return successorModel
            
        # Check if successor may join the current list of best successors
        # Or if it is good enough to kick the others from the list
        if(satClauses > maxSatClauses):
            bestSuccessors.clear()
            bestSuccessors.append(successorModel)
            maxSatClauses = satClauses
        elif (satClauses == maxSatClauses):
            bestSuccessors.append(successorModel)

    return random.choice(bestSuccessors)


##### flipRandomSymbol #####
# [purpose]: Flip Random Symbol from clause.
#
# [input]:
#
#   clause - clause from which the symbol
#   will be selected
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: Resulting successor model.
#
def flipRandomSymbol(clause, model):

    # Choose a random literal
    randomLiteral = random.choice(clause)
    
    # Evaluate the symbolIndex
    symbolIndex = abs(randomLiteral)

    # Flip symbol
    model[symbolIndex] = not model[symbolIndex]

    return model

##### printSolution #####
# [purpose]: Used for debug purposes (high verbose levels).
#   Prints the assignments for each literal in each clause.
#
# [input]:
#
#   sentence - Tuple (clauses, N, C)
#
#       clauses - a list of lists of literals
#       [[l0, l1, l2], ... , [l0, l1, l2]]
#
#       N - number of variables
#
#       C - number of clauses
#
#   model - dictionary of current symbol assignments
#   { s0 : a0, ... , sN : aN }
#
# [output]: Resulting successor model.
#
def printSolution(sentence, model):

    clauses = sentence[0]
    
    for clause in clauses:

        evalList = []
        
        for literal in clause:
            symbol = abs(literal)
            evalList.append(model.get(symbol))
        
        print('{0} -> {1}'.format(clause,evalList))

    print('The value in the rightmost list is the value atributed to the symbol (Not yet evaluated in the current clause)')

