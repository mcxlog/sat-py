# dimacs.py - Provides functions for reading and writing dimacs files
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC
#

##### readDimacs #####
# [purpose]: This function reads a dimacs formated file
# and returns the problem data.
#
# [input]: fileName - name of the input file.
#
# [output]: Either the problem data or None on error.
def readDimacs(fileName):

    nline=1

    try:
        fi = open(fileName)
    except (OSError, IOError):
        print('Could not open input file '+fileName)
        return None
    
    # Ignore comments
    line = fi.readline()
    while line.startswith('c'):
        line = fi.readline()
        nline+=1

    # Check for problem line 'p'
    if not line.startswith('p'):
        return None

    problemInfo = line.split(' ')
    
    # Clean Whitespace 
    while(problemInfo.__contains__('\n')):
        problemInfo.remove('\n')
    while(problemInfo.__contains__('')):
        problemInfo.remove('')
    while(problemInfo.__contains__('\r')):
        problemInfo.remove('\r')
    
    # Enforce file formating rules
    if len(problemInfo) < 4:
        print('Error @dimacs.py: problem line ''p'' is too short')
        return None

    fileType = problemInfo[1]
    nVar = int(problemInfo[2])
    nClause = int(problemInfo[3])

    dijClauses = []

    # Enforce cnf file type
    if fileType != "cnf":
        print('Error @dimacs.py: input file not cnf')
        return None

    # Read each clause
    for i in range(int(nClause)):
        
        line = fi.readline()
        nline+=1
        
        if not line:
            print('Error @dimacs.py: clause out of bounds')
            return None # Wrong nClause

        clause = []
        
        # Fill clause
        for literal in line.split(' '):
        
            # Check for end of clause
            if literal=='0\n' or literal=='0\r\n' or literal=='0':
                break

            # Ignore Whitespace 
            if(literal=='\n' or literal=='' or literal=='\r'):
                continue
                
            # Add literal to clause 
            try:
                literal = int(literal)
            except ValueError as e:
                print('Error @dimacs.py: Value error at line %d'%nline)
                raise e
            
            if abs(literal) > nVar:
                print('Error @dimacs.py: literal out of bounds')
                return None # Wrong nVar
            
            clause.append(literal)

        # Store clause    
        dijClauses.append(clause)
    
    fi.close()
    return nVar, nClause, dijClauses

##### writeDimacs #####
# [purpose]: This function writes a dimacs formated file
# with the results from an algorithm run.
#
# [input]: fileName - name of the output file;
# results -  tuple with (<name of the algorithm>,
# <sentence>, <isSat>, <solution>,
# <exec time>, [ algorithm specific output ])
#
# [output]: None
def writeDimacs(filename, results):

    try:
        fo = open(filename,'w')
    except (OSError, IOError):
        print('Could not open output file '+filename)
        return

    algorithm = results[0]
    sentence = results[1]

    # If the algorithm evaluated True, the sentence is satisfiable
    # If DPLL evaluated False, the sentence is unsatisfiable
    # If walkSat/GSAT evaluated False, no conclusions can be taken
    if results[2] == True:
        sat = 1
    else:
        if algorithm == 'DPLL':
            sat = 0
        else:
            sat = -1
            
    # Collect model and execution time
    model = results[3]
    time = results[4]

    # Write comment lines
    print('c Algorithm used: {0}'.format(algorithm), file = fo)
    print('c Time spent: {0}'.format(time), file = fo)

    # Write algorithm specific output
    if(algorithm=='walkSAT'):
       
        seed = results[5][0]
        nFlips = results[5][1]
        print('c Random Seed used: {0}'.format(seed), file = fo)
        print('c Number of Flips: {0}'.format(nFlips), file = fo)
        
    elif(algorithm=='GSAT'):

        seed = results[5][0]
        restarts = results[5][1]
        climbs = results[5][2]
        print('c Random Seed used: {0}'.format(seed), file = fo)
        print('c Number of restarts: {0}'.format(restarts), file = fo)
        print('c Number of climbs: {0}'.format(climbs), file = fo)
        
    # Write solution lines
    print('s CNF {0} {1} {2}'.format(sat, sentence[1], sentence[2]), file = fo)

    # Write timing lines
    if(algorithm=='walkSAT'):
        print('t CNF {0} {1} {2} {3} {4}'.format(sat, sentence[1], sentence[2], time, nFlips), file = fo)
    elif(algorithm=='GSAT'):
        print('t CNF {0} {1} {2} {3} {4} {5}'.format(sat, sentence[1], sentence[2], time, restarts, climbs), file = fo)
    else:
        print('t CNF {0} {1} {2} {3} {4}'.format(sat, sentence[1], sentence[2], time, 0), file = fo)

    if results[2]==True: # If isSat
        for i,val in model.items():
            if val == True:
                print('v {0}'.format(i), file = fo)
            else:
                print('v -{0}'.format(i), file = fo)
